<?php

function getOddNumber(array $numbers)
{
    $result = 0;
    foreach ($numbers as $number) {
        $result ^= $number;
    }
    return $result;
}

class OddNumberTest extends \PHPUnit_Framework_TestCase
{
    public function dataProvider()
    {
        return [
            [[2,5,9,1,5,1,8,2,8], 9],
            [[2,3,4,3,1,4,5,1,4,2,5], 4],
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testOddNumber($input, $result)
    {
        return $this->assertEquals($result, getOddNumber($input));
    }
}



